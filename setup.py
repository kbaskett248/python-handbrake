"""Setup file."""

import setuptools

setuptools.setup(
    name="handbrake",
    version="0.1.0",
    url=" ",

    author="Kenny Baskett",
    author_email="kbaskett248@gmail.com",

    description="A python interface to the handbrake cli",
    long_description=open('README.rst').read(),

    packages=setuptools.find_packages(),

    install_requires=[],

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
    ],
)

"""Tests for handbrake"""

from handbrake import Handbrake


def test_presets():
    """Ensure that presets are determined correctly."""
    hb = Handbrake()
    assert 'Very Fast 1080p30' in hb.presets
    assert 'Android 1080p30' in hb.presets

"""handbrake - A python interface to the handbrake cli"""

__version__ = '0.1.0'
__author__ = 'Kenny Baskett <kbaskett248@gmail.com>'
__all__ = ['Handbrake']


from datetime import timedelta
import json
from pathlib import Path
import re
import subprocess
from typing import Callable, NamedTuple, Tuple, Union


FilePath = Union[str, Path]


def _ensure_path(str_or_path: FilePath) -> Path:
    if isinstance(str_or_path, str):
        return Path(str_or_path)
    return str_or_path


class Progress(NamedTuple):
    """A simple NamedTuple with information about the progress of an encode."""
    progress: float
    remaining: timedelta
    pass_: Tuple[int, int]


class Handbrake(object):
    """A class for interacting with the Handbrake CLI."""

    _presets: Tuple[str, ...] = None
    _preset_pattern = re.compile(r"^( +)(.+?)\n(\1\1.+\n)*", re.MULTILINE)

    @property
    def presets(self) -> Tuple[str, ...]:
        """Return the presets for Handbrake.

        The presets are cached so subsequent call do not requery. Presets are
        also loaded from the Handbrake GUI to ensure user presets are loaded.

        """
        if self._presets is None:
            self._presets = self._get_presets()
        return self._presets

    def _get_presets(self) -> Tuple[str, ...]:
        results = self._run('--preset-list')
        return tuple(match.group(2) for match
                     in self._preset_pattern.finditer(results))

    def _run(self, *args):
        result = subprocess.run(self._make_cli_args(args),
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT,
                                check=True,
                                universal_newlines=True)
        return result.stdout

    @staticmethod
    def _make_cli_args(*args) -> Tuple[str, ...]:
        return tuple(['HandBrakeCli', '--preset-import-gui'] + list(args))

    def encode(self,
               profile: str,
               infile: FilePath,
               outfile: FilePath,
               progress_callback: Callable=None) -> None:
        """Encode the specified file with the specified profile.

        """
        inpath = _ensure_path(infile)
        outpath = _ensure_path(outfile)
        args = self._make_cli_args(
            '--preset', profile, '--json', '-i', str(inpath),
            '-o', str(outpath))

        process = subprocess.Popen(args,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT,
                                   universal_newlines=True)
        in_progress = False
        progress = ''
        while True:
            output = process.stdout.readline()
            if output == '' and process.poll() is not None:
                break

            if 'Progress:' in output:
                in_progress = True
                progress = '{'
            elif in_progress:
                progress += output
                try:
                    progress_dict = json.loads(progress)
                except json.JSONDecodeError:
                    continue
                else:
                    prog = Progress(
                        progress=progress_dict['Working']['Progress'],
                        remaining=timedelta(hours=progress_dict['Working']['Hours'],
                                            minutes=progress_dict['Working']['Minutes'],
                                            seconds=progress_dict['Working']['Seconds']),
                        pass_=(progress_dict['Working']['Pass'],
                               progress_dict['Working']['PassCount']))

                    if progress_callback:
                        progress_callback(prog)

                    progress = None
                    in_progress = False

def main():
    hb = Handbrake()
    hb.encode('DVD',
              r"C:\Users\kbaskett\Make_MKV\PADDINGTON_2\title00.mkv",
              r"C:\Users\kbaskett\Make_MKV\PADDINGTON_2\title01.mkv")

if __name__ == '__main__':
    main()
